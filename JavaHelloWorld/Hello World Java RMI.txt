aPassos para desenvolver uma aplica��o Hello World em Java RMI.

O pacote java.rmi.registry tem uma classe chamada LocateRegistry (http://docs.oracle.com/javase/8/docs/api/java/rmi/registry/LocateRegistry.html) e uma interface Registry (http://docs.oracle.com/javase/8/docs/api/java/rmi/registry/Registry.html). Ambas servem para acessar o servi�o de nomes do Java RMI.
Para iniciar o servi�o de nomes, pode-se (1) chamar o execut�vel rmiregistry via linha de comando (2) chamar rmiregistry dentro do c�digo atrav�s do m�todo exec() da classe Runtime ou (3) usar a classe LocateRegistry. Quero que voc�s usem a terceira op��o. Se optarem por usar as op��es (1) e (2) � necess�rio chamar Naming.metodo() como nos slides vistos em sala.

1. Para iniciar um servi�o de nomes no servidor:
Registry referenciaServicoNomes = LocateRegistry.createRegistry(int port);
De posse da refer�ncia do Servi�o de Nomes (SN), pode-se chamar seus m�todos bind(), rebind(), unbind(), lookup() e list() --> ver classe Naming http://docs.oracle.com/javase/8/docs/api/java/rmi/Naming.html) ou interface Registry.

2. Para o cliente obter uma refer�ncia do SN, utiliza-se o m�todo getRegistry() da classe LocateRegistry: Registry referenciaServicoNomes = LocateRegistry.getRegistry(String maquinaServidor, int portaSN);

Passos de Implementa��o:
1. Criar 2 projetos distintos: um para o cliente chamado Cliente_HelloWorld e outro para o servidor chamado Servidor_HelloWorld.

2. Criar 2 pacotes distintos, um em cada projeto, ambos com o mesmo nome: um para o cliente chamado HelloWorld e outro para o servidor chamado HelloWorld.

3. Criar a interface Servidor - InterfaceServ - estendendo a interface Remote e inserir apenas um m�todo chamar() que recebe dois par�metros de entrada: uma String qualquer e a refer�ncia do cliente (tipo InterfaceCli);

4. Criar a interface Cliente - InterfaceCli - estendendo a interface Remote e inserir apenas um m�todo echo() que recebe um par�metro de entrada: uma String qualquer;

5. Criar a classe servente do servidor - ServImpl - que implementa a interface InterfaceServ e estende a classe UnicastRemoteObject.
Quando um cliente invocar o m�todo chamar(), o servidor receber� uma string qualquer e a refer�ncia do cliente e invocar� o m�todo echo();

6. Criar a classe servente do cliente - CliImpl - que implementa a interface InterfaceCli e estende a classe UnicastRemoteObject.
Quando um servidor invocar o m�todo echo(), o cliente apenas mostrar� a string recebida na tela. Nessa classe o cliente deve ter a refer�ncia do servidor e com esta ele poder� chamar o m�todo chamar() do servidor e passar� uma string qualquer e sua refer�ncia (estando em CliImpl, basta passar this);

7. Criar classe Servidor com m�todo main que vai: iniciar o servi�o de nomes (usar preferencialmente a classe LocateRegistry e m�todo createRegistry), criar uma inst�ncia da classe ServImpl e registrar a refer�ncia da sua aplica��o (tipo InterfaceServ) no servi�o de nomes. Obs: a refer�ncia � o resultado da cria��o da inst�ncia de ServImpl;

8. Criar classe Cliente com m�todo main que vai: obter refer�ncia do servi�o de nomes que est� executando no servidor (usar preferencialmente a classe LocateRegistry e m�todo getRegistry), criar uma inst�ncia da classe CliImpl passando como argumento a refer�ncia do SN.

ATEN��O: as interfaces do cliente e do servidor devem estar as duas nos dois pacotes -desafio de abertura - divulga��o de interfaces.
Existem v�rias outras formas de implementar, mas vamos fazer assim para facilitar o entendimento.

Boa sorte!!!
Cristina.